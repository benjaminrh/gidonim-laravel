<?php

class Add_Cemetery_Status_And_Comments {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cemeteries', function($table) {
			$table->integer('status');
			$table->string('comments_en', 4000);
			$table->string('comments_he', 4000);

		});

		DB::table('cemeteries')->update(array(
			'status' => 1,
		));
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cemeteries', function($table) {
			$table->drop_column('status');
			$table->drop_column('comments_en');
			$table->drop_column('comments_he');
		});
	}

}