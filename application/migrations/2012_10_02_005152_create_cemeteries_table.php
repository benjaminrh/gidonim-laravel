<?php

class Create_Cemeteries_Table {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cemeteries', function($table)
		{
			$table->increments('id');
			$table->string('title_en');
			$table->string('title_he');
			$table->string('slug', 50)->unique();
			$table->string('map');
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cemeteries');
	}

}
