<?php

class Create_Locations_Table {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('locations', function($table)
		{
			$table->increments('id');
			$table->integer('tombstone_id');
			$table->string('number', 20);
			$table->integer('block');
			$table->integer('line');
			$table->integer('tombstone');
			$table->boolean('near')->default(false);
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('locations');
	}

}
