<?php

class Create_Tombstones_Table {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tombstones', function($table)
		{
			$table->increments('id');
			$table->integer('cemetery_id');
			$table->string('number', 20);
			$table->string('first_name');
			$table->string('father_name');
			$table->string('spouse_name');
			$table->string('family_name');
			$table->string('death');
			$table->string('comments_en', 4000);
			$table->string('comments_he', 4000);
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tombstones');
	}

}
