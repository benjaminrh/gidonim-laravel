<?php

class Add_Tombstone_Image {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tombstones', function($table) {
			$table->string('photo');
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tombstones', function($table) {
			$table->drop_column('photo');
		});
	}

}