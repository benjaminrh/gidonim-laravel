<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Simply tell Laravel the HTTP verbs and URIs it should respond to. It is a
| breeze to setup your application using Laravel's RESTful routing and it
| is perfectly suited for building large applications and simple APIs.
|
| Let's respond to a simple GET request to http://example.com/hello:
|
|		Route::get('hello', function()
|		{
|			return 'Hello World!';
|		});
|
| You can even respond to more than one URI:
|
|		Route::post(array('hello', 'world'), function()
|		{
|			return 'Hello World!';
|		});
|
| It's easy to allow URI wildcards using (:num) or (:any):
|
|		Route::put('hello/(:any)', function($name)
|		{
|			return "Welcome, $name.";
|		});
|
*/

// Main
Route::get('/', 'main@index');
Route::get('home', function() {
	return Redirect::to('/');
});
Route::any('contact', 'main@contact');

// User
Route::get('users', 'users@index');
Route::any('users/add', 'users@add');
Route::any('users/edit/(:num)', 'users@edit');
Route::any('users/delete/(:num)', 'users@delete');

// Auth
Route::any('login', 'auth@login');
Route::get('logout', 'auth@logout');

// Tombstones
Route::get('cemeteries/(:any)/tombstones', 'tombstones@index');
Route::any('cemeteries/(:any)/tombstones/add', 'tombstones@add');
Route::any('cemeteries/(:any)/tombstones/(:any)/edit', 'tombstones@edit');
Route::any('cemeteries/(:any)/tombstones/(:any)/delete', 'tombstones@delete');
Route::get('cemeteries/(:any)/tombstones/(:any)', 'tombstones@detail');

// Cemeteries
Route::get('cemeteries', 'cemeteries@index');
Route::any('cemeteries/add', 'cemeteries@add');
Route::any('cemeteries/(:any)/edit', 'cemeteries@edit');
Route::any('cemeteries/(:any)/delete', 'cemeteries@delete');
Route::get('cemeteries/(:any)', 'cemeteries@detail');

/*
|--------------------------------------------------------------------------
| Application 404 & 500 Error Handlers
|--------------------------------------------------------------------------
|
| To centralize and simplify 404 handling, Laravel uses an awesome event
| system to retrieve the response. Feel free to modify this function to
| your tastes and the needs of your application.
|
| Similarly, we use an event to handle the display of 500 level errors
| within the application. These errors are fired when there is an
| uncaught exception thrown in the application.
|
*/

Event::listen('404', function()
{
	return Response::error('404');
});

Event::listen('500', function()
{
	return Response::error('500');
});

/*
|--------------------------------------------------------------------------
| Route Filters
|--------------------------------------------------------------------------
|
| Filters provide a convenient method for attaching functionality to your
| routes. The built-in before and after filters are called before and
| after every request to your application, and you may even create
| other filters that can be attached to individual routes.
|
| Let's walk through an example...
|
| First, define a filter:
|
|		Route::filter('filter', function()
|		{
|			return 'Filtered!';
|		});
|
| Next, attach the filter to a route:
|
|		Router::register('GET /', array('before' => 'filter', function()
|		{
|			return 'Hello World!';
|		}));
|
*/

Route::filter('before', function()
{
	// Do stuff before every request to your application...

	// Default language ($lang) & current uri language ($lang_uri)
	$lang = 'en';
	$lang_uri = URI::segment(1);

	// Set default session language if none is set
	if(!Session::has('language'))
	{
		Session::put('language', $lang);
	}

	// Route language path if needed
	if($lang_uri !== 'en' && $lang_uri !== 'he')
	{
		return Redirect::to($lang.'/'.($lang_uri ? URI::current() : ''));
	}
	// Set session language to uri
	elseif($lang_uri !== Session::get('language'))
	{
		Session::put('language', $lang_uri);
	}

	// Store the language switch links to the session
	$he2en = preg_replace('/he\//', 'en/', URI::full(), 1);
	$en2he = preg_replace('/en\//', 'he/', URI::full(), 1);
	Session::put('he2en', $he2en);
	Session::put('en2he', $en2he);

	// For page redirection upon login and logout
	if(URI::current() !== 'login') {
		Session::put('destination', url(URI::current()));
	}
});

Route::filter('after', function($response)
{
	// Do stuff after every request to your application...
});

Route::filter('csrf', function()
{
	if (Request::forged()) return Response::error('500');
});

Route::filter('auth', function()
{
	if (Auth::guest()) {
		Session::reflash();
		return Redirect::to('login');
	}
});

Route::filter('c_prep', function() {
	if (URI::current() !== 'cemeteries') {
		$cemetery = Cemetery::find_by_slug(URI::segment(3));
		if(!$cemetery) {
			return Response::error('404');
		}
	}
});

Route::filter('t_prep', function() {
	$cemetery = Cemetery::find_by_slug(URI::segment(3));
	
	if (!$cemetery || $cemetery->status == 3 || $cemetery->status == 4) {
		return Response::error('404');
	} else if ($cemetery->status == 2 && Auth::guest()) {
		// Under Construction cemetery
		return Redirect::to('login')->with('status-error', 'You must login to view this cemetery.');
	}

	if (URI::segment(5)) {
		$tombstone = $cemetery->tombstones()->where_number(URI::segment(5))->first();
		if (!$tombstone) {
			return Response::error('404');
		}
	}
});