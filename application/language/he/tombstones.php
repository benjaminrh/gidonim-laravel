<?php

return array(
	'tombstones' => 'מצבות',
	'tombstone' => 'מצבה',
	'no-results' => 'לא נמצא מצבות.',
	'location' => 'מקום',
	'block' => 'בלוק',
	'line' => 'שורה',
	'near' => 'ליד',
	'number' => 'מס\' מצבה',
	'first_name' => 'שם פרטי',
	'father_name' => 'שם האב',
	'spouse_name' => 'בן זוג',
	'family_name' => 'שם משפחה',
	'death' => 'תאריך פטירה',
	'comments_en' =>'התגובות (באנגלית)',
	'comments_he' =>'התגובות (בעברית)',
	'photo' => 'תצלום',
	'add_tombstone' => 'הוסף מצבה',
	'edit_tombstone' => 'ערוך מצבה',
	'delete_tombstone' => 'אישור מחיק',
	'delete_tombstone_info' => 'האם אתה בטוח שברצון למחוק לצמיתות המצבה הזה? לא ניתן לבטל.',
	'delete_tombstone_acceptance' => 'כן, מחק את המצבה הזה'
);