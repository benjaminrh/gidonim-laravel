<?php

return array(
	'nav' => 'Cemeteries',
	'cemeteries' => 'Cemeteries',
	'cemetery' => 'Cemetery',
	'no-results' => 'No cemeteries were found.',
	'status-1' => 'Documented',
	'status-2' => 'Under Construction',
	'status-3' => 'Mass Grave',
	'status-4' => 'No Records',
	'status-5' => 'Partial',
	'slug' => 'Slug (url)',
	'title_en' => 'Title (English)',
	'title_he' => 'Title (Hebrew)',
	'status' => 'Status',
	'map' => 'Map',
	'comments_en' => 'Comments (English)',
	'comments_he' => 'Comments (Hebrew)',
	'comments' => 'Comments',
	'add_cemetery' => 'Add cemetery',
	'edit_cemetery' => 'Edit cemetery',
	'delete_cemetery' => 'Delete Cemetery',
	'delete_cemetery_info' => 'Are you sure you wish to permanently delete this cemetery? This will also remove all of the tombstones associated with this cemetery, and cannot be undone.',
	'delete_cemetery_acceptance' => 'Yes, delete this cemetery'
);