<?php

return array(
	'tombstones' => 'Tombstones',
	'tombstone' => 'Tombstone',
	'no-results' => 'No tombstones were found.',
	'location' => 'Location',
	'block' => 'Block',
	'line' => 'Line',
	'near' => 'Near',
	'number' => 'Number',
	'first_name' => 'First Name',
	'father_name' => 'Father\'s Name',
	'spouse_name' => 'Spouse',
	'family_name' => 'Family Name',
	'death' => 'Date of Demise',
	'comments_en' =>'Comments (English)',
	'comments_he' =>'Comments (Hebrew)',
	'photo' => 'Photo',
	'add_tombstone' => 'Add tombstone',
	'edit_tombstone' => 'Edit tombstone',
	'delete_tombstone' => 'Delete tombstone',
	'delete_tombstone_info' => 'Are you sure you wish to permanently delete this tombstone? This cannot be undone.',
	'delete_tombstone_acceptance' => 'Yes, delete this tombstone'
);