<?php

class Cemeteries_Controller extends Base_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->filter('before', 'auth')->only(array('add', 'edit', 'delete'));
		$this->filter('before', 'c_prep');

		if (URI::current() !== 'cemeteries') {
			$this->cemetery = Cemetery::find_by_slug(URI::segment(3));
			if ($this->cemetery) {
				$this->title = (Session::get('language') == 'he' ? $this->cemetery->title_he : $this->cemetery->title_en);
			}
		}
	}

	// public function before() { MORE DRY!!!
	// 	if (URI::current() !== 'cemeteries') {
	// 		$this->cemetery = Cemetery::find_by_slug(URI::segment(3));
	// 		if(!$this->cemetery) {
	// 			return Response::error('404');
	// 		}
	// 		$this->title = (Session::get('language') == 'he' ? $this->cemetery->title_he : $this->cemetery->title_en);
	// 	}
	// }

	public function get_index()
	{
		// PAGE - List of cemeteries
		$order_by = (Session::get('language') == 'he' ? 'title_he' : 'title_en');
		$cemeteries = Cemetery::order_by($order_by, 'asc')->get();

		return View::make('cemetery.index')->with('cemeteries', $cemeteries);
	}

	public function get_detail($cemetery_slug)
	{
		// PAGE - Single cemetery view
		$cemetery = $this->cemetery;

		switch ($cemetery->status) {
			case 5:
			case 1:
				// Documented and partial (normal) cemetery page
				return View::make('cemetery.show')->with('cemetery', $cemetery);
				break;
			case 2:
				// Under Construction cemetery page
				if (Auth::check()) {
					return View::make('cemetery.show')->with('cemetery', $cemetery);
				} else {
					return Redirect::to('login')->with('status-error', 'You must login to view this cemetery.');
				}
				break;
			case 3:
				// Mass Grave cemetery page
				return View::make('cemetery.show-mass_grave')->with('cemetery', $cemetery);
				break;
			case 4:
				// No Records cemetery page
				return View::make('cemetery.show-no_records')->with('cemetery', $cemetery);
				break;
			default:
				// For some reason, the cemetery status is not defined
				return Response::error('500');
		}
	}

	public function get_add()
	{
		// PAGE - Add a new cemetery
		$cemetery = new Cemetery; // Initiate for cleaner form view setup
		return View::make('cemetery.new')->with('cemetery', $cemetery);
	}

	public function post_add()
	{
		// HANDLE - Add a new cemetery
		$input = Input::only(Cemetery::$accessible);
		Input::flash();
		$validation = Validator::make($input, Cemetery::$validation_rules);

		if($validation->fails()) {
			$cemetery = new Cemetery; // Initiate for cleaner form view setup
			return Redirect::back()->with_input()->with_errors($validation)->with('cemetery', $cemetery);
		} else {
			// Add cemetery to database
			$cemetery = Cemetery::create_cemetery($input);

			$title = (Session::get('language') == 'he' ? $cemetery->title_he : $cemetery->title_en);

			return Redirect::to('cemeteries/'.$cemetery->slug)->with('status', $title.' has been saved!');
		}
	}

	public function get_edit($cemetery_slug)
	{
		// PAGE - Edit a cemetery
		$cemetery = $this->cemetery;

		return View::make('cemetery.edit')->with('cemetery', $cemetery);
	}

	public function post_edit($cemetery_slug)
	{
		// HANDLE - Edit a cemetery
		$input = Input::only(Cemetery::$accessible);
		Input::flash();
		$cemetery = $this->cemetery;
		$rules = Cemetery::$validation_rules;
		$rules['slug'] .= ','.$cemetery->id;
		$validation = Validator::make($input, $rules);

		if($validation->fails()) {
			return Redirect::back()->with_input()->with_errors($validation)->with('cemetery', $cemetery);
		} else {
			// Update cemetery database entry
			$cemetery->update_cemetery($input);

			$title = (Session::get('language') == 'he' ? $cemetery->title_he : $cemetery->title_en);

			return Redirect::to('cemeteries')->with('status', 'Your changes to '.$title.' have been saved!');
		}
	}

	public function get_delete($cemetery_slug)
	{
		// PAGE - Confirm cemetery deletion
		$cemetery = $this->cemetery;

		$title = (Session::get('language') == 'he' ? $cemetery->title_he : $cemetery->title_en);

		return View::make('cemetery.delete')->with('title', $title);
	}

	public function post_delete($cemetery_slug)
	{
		// HANDLE - Delete the cemetery if deletion is confirmed
		$input = Input::only(array('delete-confirm'));
		$rules = array('delete-confirm' => 'accepted');
		$validation = Validator::make($input, $rules);
		$cemetery = $this->cemetery;
		$title = (Session::get('language') == 'he' ? $cemetery->title_he : $cemetery->title_en);

		if($validation->fails()) {
			return Redirect::back()->with_errors($validation)->with('title', $title);
		}

		$cemetery->delete();

		return Redirect::to('cemeteries')->with('status', 'Cemetery '.$title.' has been deleted.');
	}
}