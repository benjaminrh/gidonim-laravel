<?php

class Tombstones_Controller extends Base_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->filter('before', 'auth')->only(array('add', 'edit', 'delete'));
		$this->filter('before', 't_prep');

		$this->cemetery = Cemetery::find_by_slug(URI::segment(3));

		if ($this->cemetery) {
			$this->c_title = (Session::get('language') == 'he' ? $this->cemetery->title_he : $this->cemetery->title_en);

			if (URI::segment(5)) {
				$this->tombstone = $this->cemetery->tombstones()->where_number(URI::segment(5))->first();

				if ($this->tombstone) {
					$this->t_title = $this->c_title.' : '.$this->tombstone->number;	
				}
			}
		}
	}

	// public function before() { MORE DRY!!!
	// 	$this->cemetery = Cemetery::find_by_slug(URI::segment(3));
		
	// 	if (!$this->cemetery || $this->cemetery->status == 3 || $this->cemetery->status == 4) {
	// 		return Response::error('404');
	// 	} else if ($this->cemetery->status == 2 && Auth::guest()) {
	// 		// Under Construction cemetery
	// 		return Redirect::to('login')->with('status-error', 'You must login to view this cemetery.');
	// 	}

	// 	$this->c_title = (Session::get('language') == 'he' ? $this->cemetery->title_he : $this->cemetery->title_en);

	// 	if (URI::segment(5)) {
	// 		$this->tombstone = $this->cemetery->tombstones()->where_number(URI::segment(5))->first();
	// 		if (!$this->tombstone) {
	// 			return Response::error('404');
	// 		}
	// 		$this->t_title = $this->c_title.' : '.$this->tombstone->number;
	// 	}
	// }

	public function get_index($cemetery_slug)
	{
		// PAGE - List of tombstones
		$cemetery = $this->cemetery;

		$tombstones = $cemetery->tombstones()->order_by('number', 'asc')->get();

		return View::make('tombstone.index')
			->with('data', array(
				'title' => $this->c_title,
				'tombstones' => $tombstones,
				'cemetery_slug' => $cemetery_slug
			));
	}

	public function get_detail($cemetery_slug, $tombstone_number)
	{
		// PAGE - Single tombstone view
		$cemetery = $this->cemetery;
		$tombstone = $this->tombstone;
		$location = $tombstone->location;

		return View::make('tombstone.show')->with('tombstone', $tombstone)->with('title', $this->t_title)->with('location', $location);
	}

	public function get_add($cemetery_slug)
	{
		// PAGE - Add a tombstone
		$tombstone = new Tombstone; // Initiate for cleaner form view setup
		
		return View::make('tombstone.new')->with('tombstone', $tombstone)->with('title', $this->c_title . ' > New Tombstone');
	}

	public function post_add($cemetery_slug)
	{
		// HANDLE - Add a new tombstone
		$input = Input::only(Tombstone::$accessible);
		Input::flash();
		$cemetery = $this->cemetery;
		$rules = Tombstone::$validation_rules;
		$rules['number'] .= '|unique_cemetery:'.$cemetery->id;
		$validation = Validator::make($input, $rules);

		if ($validation->fails()) {
			$tombstone = new Tombstone; // Initiate for cleaner form view setup
			return Redirect::back()->with_input()->with_errors($validation)->with('tombstone', $tombstone)->with('title', $this->c_title);
		} else {
			// Add tombstone to database
			$tombstone = Tombstone::create_tombstone($cemetery, $input);
			
			return Redirect::to('cemeteries/'.$cemetery_slug.'/tombstones/'.$tombstone->number)->with('status', 'Tombstone #'.$tombstone->number.' has been saved!');
		} 
	}

	public function get_edit($cemetery_slug, $tombstone_number)
	{
		// PAGE - Edit a tombstone
		$cemetery = $this->cemetery;
		$tombstone = $this->tombstone;

		return View::make('tombstone.edit')->with('title', $this->t_title)->with('tombstone', $tombstone);
	}

	public function post_edit($cemetery_slug, $tombstone_number)
	{
		// HANDLE - Edit a tombstone
		$input = Input::only(Tombstone::$accessible);
		Input::flash();
		$cemetery = $this->cemetery;
		$tombstone = $this->tombstone;
		$rules = Tombstone::$validation_rules;
		$rules['number'] .= '|unique_cemetery:'.$cemetery->id.','.$tombstone->id;
		$validation = Validator::make($input, $rules);

		if ($validation->fails()) {
			return Redirect::back()->with_input()->with_errors($validation)->with('title', $this->t_title)->with('tombstone', $tombstone);
		} else {
			// Update tombstone database entry
			$tombstone->update_tombstone($cemetery, $input);

			return Redirect::to('cemeteries/'.$cemetery_slug.'/tombstones/'.$tombstone->number)->with('status', 'Tombstone '.$tombstone->number.' has been saved!');
		}
	}

	public function get_delete($cemetery_slug, $tombstone_number)
	{
		// PAGE - Confirm tombstone deletion
		$cemetery = $this->cemetery;
		$tombstone = $this->tombstone;

		return View::make('tombstone.delete')->with('title', $this->t_title);
	}

	public function post_delete($cemetery_slug, $tombstone_number)
	{
		// HANDLE - Delete the tombstone if deletion is confirmed
		$input = Input::only(array('delete-confirm'));
		$rules = array('delete-confirm' => 'accepted');
		$validation = Validator::make($input, $rules);
		$cemetery = $this->cemetery;
		$tombstone = $this->tombstone;

		if ($validation->fails()) {
			return Redirect::back()->with_errors($validation)->with('title', $this->t_title);
		}

		// Now delete the tombstone
		$number = $tombstone->number;
		$tombstone->delete();

		return Redirect::to('cemeteries/'.$cemetery_slug.'/tombstones')->with('status', 'Tombstone '.$number.' has been deleted.');
	}
}