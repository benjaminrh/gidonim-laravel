<?php

class Users_Controller extends Base_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->filter('before', 'auth');
	}

	public function get_index()
	{
		// PAGE - List of users
		$users = User::order_by('email', 'asc')->get();

		return View::make('users.index')->with('users', $users);
	}

	public function get_add()
	{
		// PAGE - Add a new user
		$user = new User; // Initiate for cleaner form view setup

		return View::make('users.new')->with('user', $user);
	}

	public function post_add()
	{
		// HANDLE - Add a new user
		$input = Input::only(User::$accessible);
		Input::flash();
		$rules = User::$validation_rules;
		$rules['password'] .= '|required';
		$rules['password_confirmation'] .= '|required';
		$validation = Validator::make($input, $rules);

		if($validation->fails()) {
			$user = new User; // Initiate for cleaner form view setup
			return Redirect::back()->with_input()->with_errors($validation)->with('user', $user);;
		}

		// Add user to database
		$user = User::create_user($input);

		return Redirect::to('users')->with('status', 'User '.$user->email.' has been saved!');
	}

	public function get_edit($user_id)
	{
		// PAGE - Edit an existing user
		$user = User::find($user_id);

		if(!$user) {
			return Response::error('404');
		}

		return View::make('users.edit')->with('user', $user);
	}

	public function post_edit($user_id)
	{
		// HANDLE - Edit an existing user
		$user = User::find($user_id);
		$input = Input::only(User::$accessible);
		Input::flash();
		$rules = User::$validation_rules;
		$rules['email'] .= ','.$user_id;
		$validation = Validator::make($input, $rules);

		if($validation->fails()) {
			return Redirect::back()->with_input()->with_errors($validation)->with('user', $user);
		}

		// Update user database entry
		$user->update_user($input);

		return Redirect::to('users')->with('status', 'Your changes to user '.$user->email.' have been saved!');
	}

	public function get_delete($user_id)
	{
		// PAGE - Confirm user deletion
		$user = User::find($user_id);

		if(!$user) {
			return Response::error('404');
		}

		return View::make('users.delete')->with('user', $user);
	}

	public function post_delete($user_id)
	{
		// HANDLE - Delete the user if deletion is confirmed
		$input = Input::only(array('delete-confirm'));
		$rules = array('delete-confirm' => 'accepted');
		$validation = Validator::make($input, $rules);
		$user = User::find($user_id);

		if($validation->fails()) {
			return Redirect::back()->with_errors($validation)->with('user', $user);
		}
		
		// Now delete the user
		$user_email = $user->email;
		$user->delete();
		return Redirect::to('users')->with('status', 'User '.$user_email.' has been deleted.');
	}
}