<?php

class Main_Controller extends Base_Controller
{
	private $contact_validation_rules = array(
		'name' => 'required',
		'email' => 'required|email',
		'subject' => 'required',
		'message' => 'required'
	);

	private $accessible = array(
		'name',
		'email',
		'subject',
		'message'
	);

	public function get_index()
	{
		// PAGE - Home
		$view = 'main.' . (Session::get('language') == 'he' ? 'home_he' : 'home_en');

		return View::make('main.home_en');
	}

	public function get_contact()
	{
		// PAGE - Contact us
		return View::make('main.contact');
	}

	public function post_contact()
	{
		// HANDLE - Contact us
		$input = Input::only($this->accessible);
		$rules = $this->contact_validation_rules;
		$validation = Validator::make($input, $rules);

		if($validation->fails()) {
			return Redirect::back()->with_input()->with_errors($validation);
		} else {
			// Send contact form

			// Get the Swift Mailer instance
			$mailer = IoC::resolve('mailer');

			// Construct the message
			$message = Swift_Message::newInstance($input['subject'])
			    ->setFrom(array($input['email']=>$input['name']))
			    ->setTo(array('benjamin3harris@gmail.com'=>'Gidonim'))
			    ->setBody($input['message'],'text/plain');

			// Send the email
			$mailer->send($message);

			return Redirect::home()->with('status', 'Your message has been sent!');
		}
	}
}