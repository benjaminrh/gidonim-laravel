<?php

class Tombstone extends Eloquent
{
	public static $timestamps = false;

	// Acceptable inputs
	public static $accessible = array(
		'number',
		'first_name',
		'father_name',
		'spouse_name',
		'family_name',
		'death',
		'comments_en',
		'comments_he',
		'photo'
	);

	// Validation rules
	public static $validation_rules = array(
		'number' => 'required|min:1000|numeric',
		'photo' => 'mimes:jpg|image'
	);

	public function cemetery()
	{
		return $this->belongs_to('Cemetery');
	}

	public function location()
	{
		return $this->has_one('Location');
	}

	public function delete() {
		$this->location()->delete(); // Delete its location
		File::delete($this->photo); // Delete the photo file
		return parent::delete(); // Now delete itself
	}

	static function create_tombstone($cemetery, $input) {
		$tombstone = new Tombstone;
		$tombstone->fill($input);
		$tombstone->cemetery_id = $cemetery->id;
		$tombstone->save();

		$files = Input::file();
		if($files['photo']['size']) {
			// Update tombstone photo
			$photo_path = path('public').'cemetery_images';
			$photo_name = $cemetery->id.'_t_'.$tombstone->id;
			Input::upload('photo', $photo_path, $photo_name);
			$tombstone->photo = 'cemetery_images/'.$photo_name;
			$tombstone->save();
		}

		// Now generate tombstone's location
		$location = new Location;
		$location->fill(Location::parse_tombstone_number($tombstone->number));
		$location->tombstone_id = $tombstone->id;
		$location->save();

		return $tombstone;
	}

	public function update_tombstone($cemetery, $input) {
		// Update the cemetery
		$this->fill($input);

		$files = Input::file();
		if($files['photo']['size']) {
			// Update tombstone photo
			$photo_path = path('public').'cemetery_images';
			$photo_name = $cemetery->id.'_t_'.$this->id;
			Input::upload('photo', $photo_path, $photo_name);
			$this->photo = 'cemetery_images/'.$photo_name;
		}

		$this->save();

		// Now update tombstone's location
		if($this->location) {
			$location = $this->location()->first();
			$location->fill(Location::parse_tombstone_number($this->number));
			$location->save();
		} else {
			// Location doesn't already exist, so generate tombstone's location
			$location = new Location;
			$location->fill(Location::parse_tombstone_number($this->number));
			$location->tombstone_id = $this->id;
			$location->save();
		}

		return $this;
	}
}