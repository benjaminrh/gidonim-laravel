<?php

class Location extends Eloquent
{
	public static $timestamps = false;
	

	public function tombstone()
	{
		return $this->belongs_to('Tombstone');
	}
	
	static function parse_tombstone_number($number)
	{
		$n = strval($number);
		if (substr($n, -2, -1) == ".") {
			$near = substr($n, -1);
			$n = substr($n, 0, -2);
		} else {
			$near = 0;
		}
		$block = substr($n, 0, -3);
		$line = substr($n, -3, 1);
		$tombstone = substr($n, -2);
		$results = array(
			'number' => $n,
			'block' => $block,
			'line' => $line,
			'tombstone' => $tombstone,
			'near' => $near
		);
		return $results;
	}
}