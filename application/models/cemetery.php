<?php

class Cemetery extends Eloquent
{
	// Disable timestamps
	public static $timestamps = false;

	// Acceptable inputs
	public static $accessible = array(
		'slug',
		'title_en',
		'title_he',
		'comments_en',
		'comments_he',
		'map',
		'status'
	);

	// Validation rules
	public static $validation_rules = array(
		'slug' => 'required|between:2,50|match:/^([a-z-])+$/i|unique:cemeteries,slug',
		'title_en' => 'required|min:2',
		'title_he' => 'required|min:2',
		'map' => 'mimes:jpg|image',
		'status' => 'required|integer|in:1,2,3,4,5'
	);
	
	public function tombstones()
	{
		return $this->has_many('Tombstone');
	}

	static function find_by_slug($slug)
	{
		return static::where_slug($slug)->first();
	}

	public function delete() {
		foreach($this->tombstones as $tombstone) {
			// Delete its tombstones
			$tombstone->delete();
		}
		File::delete($this->map); // Delete its map (image)
		return parent::delete(); // Now delete itself
	}

	static function create_cemetery($input) {
		$cemetery = new Cemetery;
		$cemetery->fill($input);
		$cemetery->save();

		$files = Input::file();
		if($files['map']['size']) {
			// Upload cemetery map
			$map_path = path('public').'cemetery_images';
			$map_name = $cemetery->id.'_map.jpg';
			Input::upload('map', $map_path, $map_name);
			$cemetery->map = 'cemetery_images/'.$map_name;
			$cemetery->save();
		}

		return $cemetery;
	}

	public function update_cemetery($input) {
		// Update the cemetery
		$this->fill($input);

		$files = Input::file();
		if($files['map']['size']) {
			// Update cemetery map
			$map_path = path('public').'cemetery_images';
			$map_name = $this->id.'_map.jpg';
			Input::upload('map', $map_path, $map_name);
			$this->map = 'cemetery_images/'.$map_name;
		}

		$this->save();
		return $this;
	}
}