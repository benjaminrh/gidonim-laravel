@layout('layouts.center_content')

@section('title')
{{ __('main.login') }}
@endsection

@section('content')
	{{ Form::vertical_open() }}
		{{ Form::token() }}
		<?php echo 
			Form::control_group(
				Form::label('email', __('main.email')),
				Form::text('email', Input::old('email')),
				($errors->has('email') ? 'error' : ''),
				Form::block_help($errors->first('email'))
			);
		?>
		<?php echo 
			Form::control_group(
				Form::label('password', __('main.password')),
				Form::password('password'),
				($errors->has('password') ? 'error' : ''),
				Form::block_help($errors->first('password'))
			);
		?>
		{{ Form::submit(__('main.login')) }}
	{{ Form::close() }}
@endsection