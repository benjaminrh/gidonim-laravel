@layout('layouts.full_content')

@section('title')
{{ __('main.edit_user') }}
@endsection

@section('title-addon')
{{ $user->email }}
@endsection

@section('content')
@include('users.form')
@endsection