@layout('layouts.full_content')

@section('title')
{{ __('main.users') }}
@endsection

@section('content')
	<table class="table-list table-hover">
		<tr class="row-add-button">
			<td colspan="2">{{ Button::success_link(url('users/add'), __('main.add_user')) }}</td>
		</tr>
		@foreach ( $users as $user )
			<tr>
				<td><b>
					{{ $user->email }}
				</b></td>
				<td>
					{{ ButtonGroup::open() }}
						{{ Button::link(url('users/edit/'.$user->id), __('main.edit')) }}
						{{ Button::danger_link(url('users/delete/'.$user->id), __('main.delete')) }}
					{{ ButtonGroup::close() }}
				</td>
			</tr>
		@endforeach
	</table>
@endsection