@layout('layouts.full_content')

@section('title')
{{ __('main.add_user') }}
@endsection

@section('content')
@include('users.form')
@endsection