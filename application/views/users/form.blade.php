{{ Form::vertical_open() }}
	{{ Form::token() }}
	<?php echo 
		Form::control_group(
			Form::label('email', __('main.email')),
			Form::text('email', Input::old('email', $user->email), array('dir'=>'ltr')),
			($errors->has('email') ? 'error' : ''),
			Form::block_help($errors->first('email'))
		);
	?>
	<p style="padding-top: 20px"><b>{{ __('main.change_password_info') }}</b></p>
	<?php echo 
		Form::control_group(
			Form::label('password', __('main.password')),
			Form::password('password'),
			($errors->has('password') ? 'error' : ''),
			Form::block_help($errors->first('password'))
		);
	?>
	<?php echo 
		Form::control_group(
			Form::label('password_confirmation', __('main.password_confirmation')),
			Form::password('password_confirmation'),
			($errors->has('password_confirmation') ? 'error' : ''),
			Form::block_help($errors->first('password_confirmation'))
		);
	?>
	{{ Form::submit(__('main.save')) }}
{{ Form::close() }}