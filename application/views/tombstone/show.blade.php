@layout('layouts.full_content')

@section('title')
{{ __('tombstones.tombstone') }}
@endsection

@section('title-addon')
{{ $title }}
@endsection

@section('content')
<div class="container">
	<div class="row">
		<div class="span4 infobar">
			<p>
				{{ Image::polaroid($tombstone->photo, '', array('style'=>'width:100%;max-height:350px')) }}
			</p>

			@if(Auth::check())
			<p>
				{{ ButtonGroup::open() }}
					{{ Button::link(url(URI::current().'/edit'), __('main.edit')) }}
					{{ Button::danger_link(url(URI::current().'/delete'), __('main.delete')) }}
				{{ ButtonGroup::close() }}
			</p>
			@endif

			<h4>{{ __('tombstones.location') }}</h4>

			<p>
				<strong>{{ ucfirst(__('tombstones.block')) }}</strong>: 
				{{ $location->block }}
			</p>

			<p>
				<strong>{{ ucfirst(__('tombstones.line')) }}</strong>: 
				{{ $location->line }}
			</p>

			<p>
				<strong>{{ ucfirst(__('tombstones.tombstone')) }}</strong>: 
				{{ $location->tombstone . ($location->near ? '.'.$location->near : '') }}
			</p>
		</div>

		<div class="span8">
			<h4>Data</h4>
		</div>
	</div>
</div>
@endsection