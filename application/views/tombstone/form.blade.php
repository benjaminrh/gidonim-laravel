{{ Form::vertical_open_for_files() }}
	{{ Form::token() }}
	<?php echo 
		Form::control_group(
			Form::label('number', __('tombstones.number')),
			Form::text('number', Input::old('number', $tombstone->number)),
			($errors->has('number') ? 'stack error' : 'stack'),
			Form::block_help($errors->first('nstackumber'))
		);
	?>
	<?php echo 
		Form::control_group(
			Form::label('first_name', __('tombstones.first_name')),
			Form::text('first_name', Input::old('first_name', $tombstone->first_name)),
			($errors->has('first_name') ? 'stack error' : 'stack'),
			Form::block_help($errors->first('first_name'))
		);
	?>
	<?php echo 
		Form::control_group(
			Form::label('father_name', __('tombstones.father_name')),
			Form::text('father_name', Input::old('father_name', $tombstone->father_name)),
			($errors->has('father_name') ? 'stack error' : 'stack'),
			Form::block_help($errors->first('father_name'))
		);
	?>
	<?php echo 
		Form::control_group(
			Form::label('spouse_name', __('tombstones.spouse_name')),
			Form::text('spouse_name', Input::old('spouse_name', $tombstone->spouse_name)),
			($errors->has('spouse_name') ? 'stack error' : 'stack'),
			Form::block_help($errors->first('spouse_name'))
		);
	?>
	<?php echo 
		Form::control_group(
			Form::label('family_name', __('tombstones.family_name')),
			Form::text('family_name', Input::old('family_name', $tombstone->family_name)),
			($errors->has('family_name') ? 'stack error' : 'stack'),
			Form::block_help($errors->first('family_name'))
		);
	?>
	<?php echo 
		Form::control_group(
			Form::label('death', __('tombstones.death')),
			Form::text('death', Input::old('death', $tombstone->death)),
			($errors->has('death') ? 'stack error' : 'stack'),
			Form::block_help($errors->first('death'))
		);
	?>
	<div class="clearfix"></div>
	<?php echo 
		Form::control_group(
			Form::label('photo', __('tombstones.photo')),
			Form::file('photo', Input::old('photo')),
			($errors->has('photo') ? 'error' : ''),
			Form::block_help($errors->first('photo'))
		);
	?>
	@if ($tombstone->photo)
		{{ HTML::image($tombstone->photo, '['.__('tombstones.photo').']', array('class' => 'img-polaroid cemetery-map')) }}
	@endif
	<div class="clearfix"></div>
	<?php echo 
		Form::control_group(
			Form::label('comments_en', __('tombstones.comments_en')),
			Form::xlarge_textarea('comments_en', Input::old('comments_en', $tombstone->comments_en), array('dir'=>'ltr', 'rows'=>5)),
			($errors->has('comments_en') ? 'stack error' : 'stack'),
			Form::block_help($errors->first('comments_en'))
		);
	?>
	<?php echo 
		Form::control_group(
			Form::label('comments_he', __('tombstones.comments_he')),
			Form::xlarge_textarea('comments_he', Input::old('comments_he', $tombstone->comments_he), array('dir'=>'rtl', 'rows'=>5)),
			($errors->has('comments_he') ? 'stack error' : 'stack'),
			Form::block_help($errors->first('comments_he'))
		);
	?>
	<div class="clearfix"></div>
	{{ Form::submit(__('main.save')) }}
{{ Form::close() }}