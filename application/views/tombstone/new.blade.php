@layout('layouts.full_content')

@section('title')
{{ __('tombstones.add_tombstone') }}
@endsection

@section('title-addon')
{{ $title }}
@endsection

@section('content')
	@include('tombstone.form')
@endsection