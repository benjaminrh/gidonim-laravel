@layout('layouts.full_content')

@section('title')
	{{ $data['title'] }}
@endsection

@section('title-addon')
	{{ __('tombstones.tombstones') }}
@endsection

@section('content')
	<table class="table-list table-hover tombstone-list">
		@if (Auth::check())
			<tr class="row-add-button <?php echo ($data['tombstones'] ? '' : 'empty'); ?>">
				<td colspan="7">{{ Button::success_link(url('cemeteries/'.$data['cemetery_slug'].'/tombstones/add'), __('tombstones.add_tombstone')) }}</td>
			</tr>
		@else
			<tr></tr>
		@endif
		@if ($data['tombstones'])
			<tr class="tombstone-head">
				<td>
					{{ __('tombstones.number') }}
				</td>
				<td class="stripe">
					{{ __('tombstones.first_name') }}
				</td>
				<td>
					{{ __('tombstones.father_name') }}
				</td>
				<td class="stripe">
					{{ __('tombstones.spouse_name') }}
				</td>
				<td>
					{{ __('tombstones.family_name') }}
				</td>
				<td class="stripe">
					{{ __('tombstones.death') }}
				</td>
				<td>
				</td>
			</tr>
		@endif
		@forelse ($data['tombstones'] as $tombstone)
		<tr>
			<td>
				{{ $tombstone->number }}
			</td>
			<td class="stripe">
				{{ $tombstone->first_name }}
			</td>
			<td>
				{{ $tombstone->father_name }}
			</td>
			<td class="stripe">
				{{ $tombstone->spouse_name }}
			</td>
			<td>
				{{ $tombstone->family_name }}
			</td>
			<td class="stripe">
				{{ $tombstone->death }}
			</td>
			<td>
				@if ( Auth::check() )
					{{ ButtonGroup::open() }}
						{{ Button::link(url('cemeteries/'.$data['cemetery_slug'].'/tombstones/'.$tombstone->number), __('main.view')) }}
						{{ Button::link(url('cemeteries/'.$data['cemetery_slug'].'/tombstones/'.$tombstone->number.'/edit'), __('main.edit')) }}
						{{ Button::danger_link(url('cemeteries/'.$data['cemetery_slug'].'/tombstones/'.$tombstone->number.'/delete'), __('main.delete')) }}
					{{ ButtonGroup::close() }}
				@else
					{{ Button::link(url('cemeteries/'.$data['cemetery_slug'].'/tombstones/'.$tombstone->number), __('main.view')) }}
				@endif
			</td>
		</tr>
	@empty
		<td colspan="7"><h5>{{ __('tombstones.no-results') }}</h5></td>
	@endforelse
	</table>
@endsection