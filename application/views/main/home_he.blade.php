@layout('layouts.main')

@section('main-content')
<div class="container">
	@if (Session::has('status'))
	{{ Alert::success('<strong>Success!</strong> '.Session::get('status')) }}
	@elseif (Session::has('status-error'))
	{{ Alert::error('<strong>Error!</strong> '.Session::get('status-error')) }}
	@endif

	<div class="hero-unit">
		<h1>{{ __('main.title') }}</h1>
		<p style="margin-top: 20px; margin-bottom: 20px">במהלך הפרוייקט תלמידים, מורים ובוגרים מ<a href="">בית ספר רעות בירושלים</a> מתעדים בתי קברות יהודיים בפולין. באתר הזה תוכלו לחפש מצבות במאגר הנתונים שלנו.</p>
		<p style="text-align: center"><a class="btn btn-primary btn-large">התמיכה גדעונים</a></p>
	</div>
</div>
@endsection