@layout('layouts.center_content')

@section('title')
{{ __('cemeteries.delete_cemetery') }}
@endsection

@section('title-addon')
{{ $title }}
@endsection

@section('content')
	{{ Form::vertical_open() }}
		{{ Form::token() }}
		<?php echo 
			Form::control_group(
				Form::label('delete-confirm', __('cemeteries.delete_cemetery_info'), array('style'=>'font-weight: bold')),
				Form::labelled_checkbox('delete-confirm', __('cemeteries.delete_cemetery_acceptance')),
				($errors->has('delete-confirm') ? 'error' : ''),
				Form::block_help($errors->first('delete-confirm'))
			);
		?>
		{{ Form::submit(__('main.delete'), array('class'=>'btn-danger', 'style'=>'margin-top: 20px')) }}
	{{ Form::close() }}
@endsection