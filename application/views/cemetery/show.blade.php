@layout('layouts.full_content')

@section('title')
	{{ __('cemeteries.cemetery') }}
@endsection

@section('title-addon')
	{{ (Session::get('language') == 'he' ? $cemetery->title_he : $cemetery->title_en) }}

	@if ($cemetery->status == 1)
		{{ Label::success(__('cemeteries.status-1')) }}
	@elseif ($cemetery->status == 2)
		{{ Label::normal(__('cemeteries.status-2')) }}
	@elseif ($cemetery->status == 5)
		{{ Label::info(__('cemeteries.status-5')) }}
	@endif
@endsection

@section('content')
<div class="container">
	<div class="row">
		<div class="span4 infobar">
			<p>
				{{ Image::polaroid($cemetery->map, '', array('style'=>'width:100%;max-height:350px')) }}
			</p>

			@if(Auth::check())
			<p>
				{{ ButtonGroup::open() }}
					{{ Button::link(url('cemeteries/'.$cemetery->slug.'/edit'), __('main.edit')) }}
					{{ Button::danger_link(url('cemeteries/'.$cemetery->slug.'/delete'), __('main.delete')) }}
				{{ ButtonGroup::close() }}
			</p>
			@endif
			
			<p>
				<strong>{{ ucfirst(__('cemeteries.cemetery')) }}</strong>: 
				{{ (Session::get('language') == 'he' ? $cemetery->title_he : $cemetery->title_en) }}
			</p>

			<p>
				<strong>{{ ucfirst(__('cemeteries.status')) }}</strong>: 
				@if ($cemetery->status == 1)
					{{ Label::success(__('cemeteries.status-1')) }}
				@elseif ($cemetery->status == 2)
					{{ Label::normal(__('cemeteries.status-2')) }}
				@elseif ($cemetery->status == 3)
					{{ Label::important(__('cemeteries.status-3')) }}
				@elseif ($cemetery->status == 4)
					{{ Label::warning(__('cemeteries.status-4')) }}
				@elseif ($cemetery->status == 5)
					{{ Label::info(__('cemeteries.status-5')) }}
				@endif
			</p>

			<p>
				{{ Button::large_link('cemeteries/'.$cemetery->slug.'/tombstones', __('tombstones.tombstones'))->block() }}
			</p>
		</div>

		<div class="span8">
			<h4>{{ __('cemeteries.comments') }}</h4>
			{{ (Session::get('language') == 'he' ? $cemetery->comments_he : $cemetery->comments_en) }}
		</div>
	</div>
</div>
@endsection