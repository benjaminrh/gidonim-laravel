{{ Form::vertical_open_for_files() }}
	{{ Form::token() }}
	<?php echo 
		Form::control_group(
			Form::label('slug', __('cemeteries.slug')),
			Form::text('slug', Input::old('slug', $cemetery->slug), array('dir'=>'ltr')),
			($errors->has('slug') ? 'stack error' : 'stack'),
			Form::block_help($errors->first('slug'))
		);
	?>
	<?php echo 
		Form::control_group(
			Form::label('title_en', __('cemeteries.title_en')),
			Form::text('title_en', Input::old('title_en', $cemetery->title_en), array('dir'=>'ltr')),
			($errors->has('title_en') ? 'stack error' : 'stack'),
			Form::block_help($errors->first('title_en'))
		);
	?>
	<?php echo 
		Form::control_group(
			Form::label('title_he', __('cemeteries.title_he')),
			Form::text('title_he', Input::old('title_he', $cemetery->title_he), array('dir'=>'rtl')),
			($errors->has('title_he') ? 'stack error' : 'stack'),
			Form::block_help($errors->first('title_he'))
		);
	?>
	<?php echo 
		Form::control_group(
			Form::label('status', __('cemeteries.status')),
			Form::select('status', array('1'=>__('cemeteries.status-1'), '2'=>__('cemeteries.status-2'), '3'=>__('cemeteries.status-3'), '4'=>__('cemeteries.status-4'), '5'=>__('cemeteries.status-5')), Input::old('comments_en', $cemetery->status)),
			($errors->has('status') ? 'stack error' : 'stack'),
			Form::block_help($errors->first('status'))
		);
	?>
	<div class="clearfix"></div>
	<?php echo 
		Form::control_group(
			Form::label('map', __('cemeteries.map')),
			Form::file('map'),
			($errors->has('map') ? 'error' : ''),
			Form::block_help($errors->first('map'))
		);
	?>
	@if ($cemetery->map)
		{{ HTML::image($cemetery->map, '['.__('cemeteries.map').']', array('class' => 'img-polaroid cemetery-map')) }}
	@endif
	<div class="clearfix"></div>
	<?php echo 
		Form::control_group(
			Form::label('comments_en', __('cemeteries.comments_en')),
			Form::xlarge_textarea('comments_en', Input::old('comments_en', $cemetery->comments_en), array('dir'=>'ltr', 'rows'=>5)),
			($errors->has('comments_en') ? 'stack error' : 'stack'),
			Form::block_help($errors->first('comments_en'))
		);
	?>
	<?php echo 
		Form::control_group(
			Form::label('comments_he', __('cemeteries.comments_he')),
			Form::xlarge_textarea('comments_he', Input::old('comments_he', $cemetery->comments_he), array('dir'=>'rtl', 'rows'=>5)),
			($errors->has('comments_he') ? 'stack error' : 'stack'),
			Form::block_help($errors->first('comments_he'))
		);
	?>
	<div class="clearfix"></div>
	{{ Form::actions(array(Button::primary_submit(__('main.save')))) }}
{{ Form::close() }}