@layout('layouts.full_content')

@section('title')
{{ __('cemeteries.edit_cemetery') }}
@endsection

@section('title-addon')
{{ (Session::get('language') == 'he' ? $cemetery->title_he : $cemetery->title_en) }}
@endsection

@section('content')
	@include('cemetery.form')
@endsection