@layout('layouts.full_content')

@section('title')
	{{ __('cemeteries.cemeteries') }}
@endsection

@section('content')
	<table class="table-list table-hover">
		@if (Auth::check())
			<tr class="row-add-button">
				<td colspan="3">{{ Button::success_link(url('cemeteries/add'), __('cemeteries.add_cemetery')) }}</td>
			</tr>
		@else
			<tr></tr>
		@endif
		@forelse ( $cemeteries as $cemetery )
		<tr>
			<td><b>
				@if ( Session::get('language') == 'he' )
					{{ $cemetery->title_he }} 
				@else
					{{ $cemetery->title_en }} 
				@endif
			</b></td>
			<td>
				@if ($cemetery->status == 1)
					{{ Label::success(__('cemeteries.status-1')) }}
				@elseif ($cemetery->status == 2)
					{{ Label::normal(__('cemeteries.status-2')) }}
				@elseif ($cemetery->status == 3)
					{{ Label::important(__('cemeteries.status-3')) }}
				@elseif ($cemetery->status == 4)
					{{ Label::warning(__('cemeteries.status-4')) }}
				@elseif ($cemetery->status == 5)
					{{ Label::info(__('cemeteries.status-5')) }}
				@endif
			</td>
			<td>
				@if ( Auth::check() )
					{{ ButtonGroup::open() }}
						{{ Button::link(url('cemeteries/'.$cemetery->slug), __('main.view')) }}
						{{ Button::link(url('cemeteries/'.$cemetery->slug.'/edit'), __('main.edit')) }}
						{{ Button::danger_link(url('cemeteries/'.$cemetery->slug.'/delete'), __('main.delete')) }}
					{{ ButtonGroup::close() }}
				@else
					{{ Button::link(url('cemeteries/'.$cemetery->slug), __('main.view')) }}
				@endif
			</td>
		</tr>
	@empty
		<td colspan="3"><h5>{{ __('cemeteries.no-results') }}</h5></td>
	@endforelse
	</table>
@endsection