@layout('layouts.full_content')

@section('title')
{{ __('cemeteries.add_cemetery') }}
@endsection

@section('content')
	@include('cemetery.form')
@endsection