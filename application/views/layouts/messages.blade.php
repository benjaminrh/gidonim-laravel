<!-- Status Messages -->
<div class="container">
	<div class="row">
		@if (Session::has('status'))
			{{ Alert::success(Session::get('status')) }}
		@endif
		@if (Session::has('status-error'))
			{{ Alert::error(Session::get('status-error')) }}
		@endif
	</div>
</div>
<!-- End Status Messages -->