<footer>
	<p>{{ __('main.footer') }}</p>
	@if (Auth::guest())
		<p>{{ HTML::link('login', __('main.login')) }}</p>
	@else
		<p><b>{{ Auth::user()->email }}</b> | {{ HTML::link('logout', __('main.logout')) }}</p>
	@endif
</footer>