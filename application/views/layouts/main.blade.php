<!DOCTYPE html>
<!--[if lt IE 7]>	  <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>		 <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>		 <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>{{ __('main.title') }}</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width">

		<style>
			body {
				padding-top: 60px;
			}
		</style>

		{{ Asset::container('bootstrapper')->styles() }}
		{{ HTML::style('css/main.css') }}

		@if ( Session::get('language') == 'he' )
			{{ HTML::style('css/rtl.css') }}
		@endif

		<!--[if lt IE 9]>
			<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
			<script>window.html5 || document.write('<script src="{{ URL::to_asset('js/vendor/html5shiv.js') }}"><\/script>')</script>
		<![endif]-->
	</head>
	<body>
		<!--[if lt IE 7]>
			<p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
		<![endif]-->

		<header>
			@include('layouts.header')
		</header>

		<div class="main_page_content">
			@yield('main-content')
		</div>
		
		<div class="container">
			@include('layouts.footer')
		</div>

		{{ Asset::container('bootstrapper')->scripts() }}
		{{ HTML::script('js/vendor/jquery.placeholder.min.js') }}
		{{ HTML::script('js/main.js') }}
	</body>
</html>