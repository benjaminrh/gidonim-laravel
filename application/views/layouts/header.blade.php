<?php
	$navbar = Navbar::create(array(), Navbar::FIX_TOP)
		->with_brand(__('main.title'), URL::to('/'))
		->collapsible()
		->autoroute(false)
		->with_menus(
			Navigation::links(
				array(
					array(__('main.home'), URL::to('/')),
					array(__('cemeteries.nav'), URL::to('cemeteries')),
					array(__('main.contact'), URL::to('contact'))
				)
			)
		)
		->with_menus(
			Navigation::links(
				// Language links
				array(
					array(__('main.change_language'), Session::get(
						Session::get('language') == 'he' ? 'he2en' : 'en2he'
					))
				)
			),
			array('class' => 'pull-right')
		);

		// User links
		if (Auth::check()) {
			$navbar->with_menus(
				Navigation::links(
					array(
						array(__('main.users'), URL::to('users')),
						array(__('main.logout'), URL::to('logout'))
					)
				),
				array('class' => 'pull-right')
			);
		}

		echo $navbar->get();
?>