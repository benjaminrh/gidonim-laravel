@layout('layouts.main')

@section('main-content')
	<div class="container">
		<div class="row" style="text-align:center">
			<h2>@yield('title')</h2>
			<h4>@yield('title-addon')</h4>
		</div>
	</div>

	@include('layouts.messages')

	<div class="container">
		<div class="row">
			<div class="span4 offset4">
				<div class="content-center">
					@yield('content')
				</div>
			</div>
		</div>
	</div>
@endsection