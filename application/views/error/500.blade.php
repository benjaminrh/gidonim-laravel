@layout('layouts.main')

@section('main-content')
	<div class="container">
		<div class="row" style="text-align:center">
			<h2>{{ __('main.500') }}</h2>
		</div>
	</div>
	<div class="container">
		<div class="row" style="text-align: center; margin-bottom: 50px">
			<p>{{ __('main.500-message') }}</p>
			<p>{{ Button::link(url('home'), __('main.home')) }}</p>
		</div>
	</div>
@endsection